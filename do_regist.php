<?php
session_start();
$gender = array(0 => "Nam", 1 => "Nữ");
$Depart = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
?>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <link rel="stylesheet" href="color_form.css">
	
</head>

<body>
	
<div class="form_center">	
	    <table class="table_form">
	     	<form method="POST" enctype='multipart/form-data'  action="<?php echo $_SERVER["PHP_SELF"];?>">
			
			<tr><td>
			<div class="form_td">Họ và tên<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div> </td>
					<td ><p class='input-form'>
                    <?php
                    echo  $_SESSION["name"];
                    ?>
                </p></td>
				</tr>
				<tr>
				<td><div class="form_td">Giới tính<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
				<td> <p class='input-form'>
                <?php
                    echo  $gender[$_SESSION["gender"]];
                    ?>
                </p>
					</td>
				</tr>
				<td><div class="form_td">Phân Khoa<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
					<td>
                <p class='input-form'>
                    <?php
                    echo $Depart[$_SESSION["group"]];
                    ?></p>	</td>
				</tr>
				<tr>
				<td><div class="form_td">Ngày sinh<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
                    <td>
                    <p class='input-form'>
                    <?php
                    echo $_SESSION["date"];
                    ?></p>
					</td>
				</tr>
			<tr>
				<td><div class="form_td">Địa chỉ<sup class="sup"></sup></div>
					<td ><p class='input-form'>
                    <?php
                    echo $_SESSION["address"];
					
                    ?></p></td>
				</tr>
				<td><div for="avatar" class="form_td">Hình ảnh<sup class="sup"> </sup></div>
				<td><div id="avatar" name="avatar">
				<?php
                            if($_SESSION["avatar"]) {
                                echo '<img src="'. $_SESSION["avatarUrl"] .'" alt="Avatar" width="150" height="100">';
                            }
                        ?>
                </div>  
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">

						<input class="form_submit" type="submit" method="POST" name="submitt" value="Xác nhận">
					</td>
				</tr>
				</form>
</table>
</body>

</html>
