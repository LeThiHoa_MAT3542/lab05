<?php
session_start();
$gender = array(0 => "Nam", 1 => "Nữ");
$Depart = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <link rel="stylesheet" href="color_form.css">
    <script type="text/javascript">
        $(function () {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
</head>
<body>

<?php
        // Create a directory
        if (!file_exists('upload')) {
            mkdir('upload', 0777, true);
        }

        // Delete directory
//        array_map('unlink', glob("upload/*.*"));
//        rmdir("upload");

        // Get String of date-time
        function getStringOfDate() {
            $date   = new DateTime(); //this returns the current date time
            $result = $date->format('Y-m-d-H-i-s');
            $krr    = explode('-', $result);
            $result = implode("", $krr);
            return $result;
        }
    ?>  
	
<?php
   $error = array();
   $data = array();
   if (!empty($_POST['submitt'])) {
	
	   $data['name'] = isset($_POST['name']) ? $_POST['name'] : '';
	   $data['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
	   $data['group'] = isset($_POST['group']) ? $_POST['group'] : '';
	   $data['date'] = isset($_POST['date']) ? $_POST['date'] : '';
	   $data['avatar'] = isset($_POST['avatar']) ? $_POST['avatar'] : '';
	   $check = false;
	   if (empty($data['name'])) {
		   $error['name'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy nhập tên.</i> ';
		   $check = false;
        	echo "<br>"; 
	   }
	   
	   if (empty($data['gender'])) {
		   $error['gender'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy chọn giới tính.</i> ';
		   $check = false;
		   echo "<br>"; 
	   }

	   if (empty($data['group'])) {
		   $error['group'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy chọn phân khoa.</i> ';
		   $check = false;
		   echo "<br>"; 
	   }
	   if(empty($data['date'])) {
		$check = false;
		   $error['date'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy nhập ngày sinh.</i> ';
	   } else {
		   $datee = explode("/",$data['date']);
		   if(!checkdate($datee[1] ,$datee[0] ,$datee[2]))
		   {
			   $error['date'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Vui lòng nhập đúng định dạng ngày sinh.</i> ';
		   $check = false;
		  
		   }

	   }
	   if(!empty($data["avatar"])) {
		$acceptable = array(
			'image/jpeg',
			'image/png'
		);

		if(!in_array($data['avatar']['type'], $acceptable) && (!empty($data["avatar"]["type"]))) {
			'<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
			Hãy tải Hình ảnh lên đúng định dạng JPEG hoặc PNG.</i> ';
		   $check = false;
		  
		}
	}

	if( $check ==true) {
		session_start();
		$_SESSION = $_POST; // Save information to Session

		$filename = $_FILES["avatar"]["tmp_name"]; // Get file

		$originalName = pathinfo($_FILES["avatar"]["name"], PATHINFO_FILENAME); // Get original file name
		$extension = pathinfo($_FILES["avatar"]["name"], PATHINFO_EXTENSION); // Get extension name of file

		$destination = "upload/" . $originalName . "_" . getStringOfDate() . "." . $extension; // Path to save image
		move_uploaded_file($filename, $destination); 

		$_SESSION['avatarUrl'] = $destination;
	}

	if(isset($_SESSION)) {
		header("Location:do_regist.php");
	}

}
?>


	<div class="form_center">	
	    <table class="table_form">
	     	<form method="POST" enctype='multipart/form-data'  action="<?php echo $_SERVER["PHP_SELF"];?>">
			<tr>
				<td class="form">
			<?php echo isset($error['name']) ? $error['name'] : ''; ?> <br />
			<?php echo isset($error['gender']) ? $error['gender'] : ''; ?> <br />
			<?php echo isset($error['group']) ? $error['group'] : ''; ?> <br />
			<?php echo isset($error['date']) ? $error['date'] : ''; ?> <br />
				</td>
			</tr>
			
		    	<tr>
				<td><div class="form_td">Họ và tên<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div> </td>
					<td ><input type="text" class="input_form" name="name" size="30"></td>
				</tr>
				
				<tr>
				<td><div class="form_td">Giới tính<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
				<td> <?php
                    for ($x = 0; $x < count($gender); $x++) {
                        echo ("
                             <input type=\"radio\" value=\"$x\"  name=\"gender\">$gender[$x]</input>
                        ");
                    } ?>

					</td>
					
				</tr>
				</tr>
				<td><div class="form_td">Phân Khoa<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
					<td>
					<select class = "selectbox_color"  name = "group" >
                    <option value=""></option>
                    <?php
                    foreach ($Depart as $key => $value) {
                        echo ("
                            <option value = \"$key\">$value</option>
                        ");
                    }
                    ?>
                </select>	</td>
				</tr>
				
				<tr>
				<td><div class="form_td">Ngày sinh<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
                    <td>
                    <input name="date" class="date form-control field__input" type="text" style="padding: 22px 12px;">
                </div>
					</td>
				</tr>
				<tr>
				<td><div class="form_td">Địa chỉ<sup class="sup"></sup></div>
					<td ><input type="text" class="input_form" name="address" size="30"></td>
				</tr>

				<tr><td>
					<div for="avatar" class="form_td">Hình ảnh<sup class="sup"></sup></div>
                   <td> <input type="file" name="avatar" id="avatar" class="input_form">

					</td>
				</tr>
				<tr>
				<td colspan="2" align="center">
						<input class="form_submit" type="submit" method="POST" name="submitt" value="Xác nhận">
					</td>
				</tr>
			</form>

		</table>
		<script type="text/javascript">  
    $('.date').datepicker({  
       format: 'dd/mm/yyyy'  
     });  
</script>
</body>
</html>